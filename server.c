/* server.c
** Name : Jeon jinwoo
** Project #1
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#define True 1 // Boolean
#define BACKLOG 10  // how many pending connections queue will hold
#define MAXDATASIZE 1024 // max number of bytes we can get at once 


void sigchld_handler(int s)
{
    while (waitpid(-1, NULL, WNOHANG) > 0)
        ;
}

// get sockaddr, IPv4 or IPv6:
void *
get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}
int main(int argc, char *argv[])
{
    int sockfd, new_fd; // listen on sock_fd, new connection on new_fd
    int numbytes; // received bytes from client
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    struct sigaction sa;
    int yes = 1;
    char s[INET6_ADDRSTRLEN];
    char buf[MAXDATASIZE]; // buffer: store messages from client
    int rv;

    int cmd; // input command from stdin
    char *port = NULL; // port number

    if (argc != 3){        
        fprintf(stderr,"number of argument is inappropriate.\n");       
        exit(1);    
    }
    
    // 0) get hostname and port number from stdin
    while( (cmd = getopt(argc, argv, "p:")) != -1){
        switch(cmd){
            case 'p':
                port = strdup(optarg);
                break;
            default:
                fprintf(stderr, "we don't support this one:%c\n", optopt);
                return 1;
        }
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP
    if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0){
        fprintf(stderr, "server: getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // 1) socket & bind: loop through all the results and bind to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next){
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
            //fprintf(stderr, "server: trying to find socket\n");
            continue;
        }
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
            fprintf(stderr, "server: failed to set sockopt\n");
            exit(1);
        }
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1){
            close(sockfd);
            //fprintf(stderr, "server: trying to bind\n");
            continue;
        }
        break;
    }
    if (p == NULL)
    {
        fprintf(stderr, "server: failed to bind.  Address maybe already in use\n");
        return 2;
    }
    freeaddrinfo(servinfo); // all done with this structure
    if (listen(sockfd, BACKLOG) == -1){
        fprintf(stderr, "server: failed to listen\n");
        exit(1);
    }

    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1){
        fprintf(stderr, "server: failed to sigaction\n");
        exit(1);
    }

    // 2) accept:
    while (True)
    { 
        sin_size = sizeof their_addr;
        new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

        if (new_fd == -1){
            //fprintf(stderr, "server: trying to accept\n");
            continue;
        }
        inet_ntop(their_addr.ss_family,
                  get_in_addr((struct sockaddr *)&their_addr),
                  s, sizeof s);

        // 3) receive messages from clients
        memset(buf, 0, MAXDATASIZE);

        pid_t pid = fork();
        if (!pid){      // this is the child process
            close(sockfd); // child doesn't need the listener
            while (True){
                numbytes = recv(new_fd, buf, MAXDATASIZE , 0);
                if (numbytes == -1){
                    fprintf(stderr, "server: failed to receive\n");
                    exit(1);
                }
                else if (numbytes == 0){ // client is finished
                    close(new_fd);
                    exit(1);
                }
                buf[numbytes] = '\0';
                
                printf("%s", buf);
                memset(buf, 0, MAXDATASIZE);
            }
        } // client is finished

        close(new_fd); // parent doesn't need this
        
    }
    return 0;
}