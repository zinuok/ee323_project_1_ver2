/* client.c
** Name : Jeon jinwoo
** Project #1
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <assert.h>

#define True 1 // Boolean
#define MAXDATASIZE 1024 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6: 
void *get_in_addr(struct sockaddr *sa){    
    if (sa->sa_family == AF_INET){        
        return &(((struct sockaddr_in*)sa)->sin_addr);    
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr); 
}

/*
 * find control sequence from each line
 * return flag:
 * flag = -1 => only single 'ENTER' is in buffer
 * flag = 1 => buffer is end with 'ENTER'
 * flag = 2 => now become 'ENTER' twice
 * flag = 0 => buffer isn't end with 'ENTER'
 */ 
int find_control_seq(char *buf, int was_ENT)
{
    assert(buf != NULL);
    if ((was_ENT == 1 || was_ENT == -1)&& buf[0] == '\n') // 'ENTER' twice ?
        return 2;


    size_t buf_len = strlen(buf);
    assert(buf_len > 0);
    char last_char = buf[buf_len-1];

    if (last_char == '\n'){
        if (buf_len == 1) return -1;
        else return 1;
    }

    return 0;
}

/*
 * send messages to server
 * returns if client should exit
 */
int message_handler(int sockfd, char *buf)
{
    int is_EOF = 0;
    int is_ENT = 0; // if 'ENTER' is at end of a line.

    memset(buf, 0, MAXDATASIZE);
    while (True){
        // check if EOF
        if (feof(stdin)){
            is_EOF = 1;
            break;
        }
        // get a line from stdin
        if (fgets(buf, MAXDATASIZE, stdin) == NULL)
            break;
        
        
        is_ENT = find_control_seq(buf, is_ENT);

        if (is_ENT == 2) // if control sequence, exit.
            return 1;
        else if (is_ENT == -1) // if single ENTER, continue.
            continue;

        if (send(sockfd, buf, MAXDATASIZE, 0) == -1){
            fprintf(stderr, "client: failed to send message\n");
            exit(1);
        }

        memset(buf, 0, MAXDATASIZE);
    }

    if (is_EOF == 1) 
        return 1;
    return 0;
}


int main(int argc, char *argv[]){    
    int sockfd, numbytes;         
    char buf[MAXDATASIZE]; // buffer: store input from stdandard input 
    struct addrinfo hints, *servinfo, *p;    
    int rv;    
    char s[INET6_ADDRSTRLEN];
    
    int cmd; // input command from stdin
    char *host = NULL; // hostname
    char *port = NULL; // port number

    if (argc != 5){        
        fprintf(stderr,"number of argument is inappropriate.\n");        
        exit(1);    
    }
    
    // flag for each option was already came
    int was_h = 0; 
    int was_p = 0; 
    
    // 0) get hostname and port number from stdin
    while( (cmd = getopt(argc, argv, "h:p:")) != -1){
        switch(cmd){
            case 'h':
                if (was_h){
                    fprintf(stderr, "client: Invalied argument\n");
                    return 1;
                }
                host = strdup(optarg);
                was_h = 1;
                break;
            case 'p':
                if (was_p){
                    fprintf(stderr, "client: Invalied argument\n");
                    return 1;
                }
                port = strdup(optarg);
                was_p =1;
                break;
            default:
                fprintf(stderr, "we don't support this one:%c\n", optopt);
                return 1;
                //break;
        }
    }
    
    
    memset(&hints, 0, sizeof hints);    
    hints.ai_family = AF_UNSPEC;    
    hints.ai_socktype = SOCK_STREAM;
    
    if ((rv = getaddrinfo(host, port, &hints, &servinfo)) != 0) {        
        fprintf(stderr, "client: getaddrinfo: %s\n", gai_strerror(rv));        
        return 1;    
    }

      
    for(p = servinfo; p != NULL; p = p->ai_next) { 
        // 1) socket: loop through all the results and connect to the first we can         
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {            
            //fprintf(stderr, "client: Trying to find socket\n");            
            continue;        
        }

        // 2) connect:  
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {            
            close(sockfd);            
            //fprintf(stderr, "client: Trying to connect\n");            
            continue;        
        }
        break;    
    }

    if (p == NULL) {        
        fprintf(stderr, "client: failed to connect.\n");        
        return 2;    
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);    
    //printf("client: connecting to %s\n", s);
    freeaddrinfo(servinfo); // all done with this structure


    // 3) send msseges to server
    int is_exit = 0;
    is_exit = message_handler(sockfd, buf);


    close(sockfd);

    return 0; 
} 